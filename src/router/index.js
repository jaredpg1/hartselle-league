import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store/index'
import firebase from 'firebase'
import Home from '@/views/Home.vue'
import About from '@/views/About.vue'
import Teams from '@/views/Teams.vue'
import Schedule from '@/views/Schedule.vue'
import Standings from '@/views/Standings.vue'
import Tournament from '@/views/Tournament.vue'
import Admin from '@/views/Admin/Admin.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'app',
    component: Home,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/home',
    name: 'home',
    component: Home,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/about',
    name: 'about',
    component: About,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/teams',
    name: 'teams',
    component: Teams,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/schedule',
    name: 'schedule',
    component: Schedule,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/standings',
    name: 'standings',
    component: Standings,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/tournament',
    name: 'tournament',
    component: Tournament,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/admin',
    name: 'admin',
    component: Admin,
    meta: {
      requiresAuth: true
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser
  if (!currentUser) {
    store.dispatch('userinfo/refreshUserState', false)
  } else {
    store.dispatch('userinfo/refreshUserState', true)
  }
  const requiresAuth = to.matched.some((record) => record.meta.requiresAuth)

  if (requiresAuth && !currentUser) {
    next('/')
  } else {
    next()
  }
})

export default router
