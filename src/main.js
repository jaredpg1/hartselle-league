import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Buefy from 'buefy'
// import 'buefy/dist/buefy.css'
import 'firebase/firestore'
import 'firebase/auth'
import firebase from 'firebase/app'
import { firestorePlugin } from 'vuefire'

Vue.config.productionTip = false

Vue.use(Buefy)
Vue.use(firestorePlugin)

let app = ''
const firebaseConfig = {
  apiKey: 'AIzaSyCV_1r-PKr7X9KNz8SU4T4Fahfee0ruoPo',
  authDomain: 'hartselle-league.firebaseapp.com',
  databaseURL: 'https://hartselle-league.firebaseio.com',
  projectId: 'hartselle-league',
  storageBucket: 'hartselle-league.appspot.com',
  messagingSenderId: '443172443704',
  appId: '1:443172443704:web:bd31156c90beee632e78cd',
  measurementId: 'G-T1VDZG2JL8'
}

const firebaseApp = firebase.initializeApp(firebaseConfig)

export const db = firebaseApp.firestore()

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: (h) => h(App)
    }).$mount('#app')
  }
})
