const getters = {
  getUserState (state) {
    return state.isUserSignedIn
  }
}
const actions = {
  refreshUserState ({ commit }, userState) {
    commit('setUserState', userState)
  }
}
const mutations = {
  setUserState (state, userState) {
    state.isUserSignedIn = userState
  }
}
const state = {
  isUserSignedIn: false
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
